#!/usr/bin/env bash

RELEASE_NOTE=$(git log -1 --oneline --format=%s)

curl -sL https://firebase.tools | upgrade=true bash

firebase appdistribution:distribute build/app/outputs/flutter-apk/app-release.apk   \
    --app "$APP_ID" \
    --release-notes "$RELEASE_NOTE" --testers-file testers.txt